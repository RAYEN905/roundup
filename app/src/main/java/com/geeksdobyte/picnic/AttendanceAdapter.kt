package com.geeksdobyte.picnic


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_list.view.*

class AttendanceAdapter(val messages: ArrayList<AttendanceModel>, val itemClick: (AttendanceModel) -> Unit) :
        RecyclerView.Adapter<AttendanceAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_list, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindForecast(messages[position])
    }

    override fun getItemCount() = messages.size

    class ViewHolder(view: View, val itemClick: (AttendanceModel) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bindForecast(message: AttendanceModel) {
            with(message) {
                itemView.evName.text = message.username
                itemView.evDate.text = ""
                Picasso.with(itemView.context).load(message.photourl)
                        .into(itemView.UserImageView)
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}