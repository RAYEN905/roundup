package com.geeksdobyte.picnic

data class DashboardModel(val dashboard: List<DashboardItem>? = null)