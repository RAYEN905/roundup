package com.geeksdobyte.picnic

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.WindowManager
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_attendance.*

class AttendanceActivity : AppCompatActivity() {
    lateinit var mFriendList: RecyclerView
    lateinit var mFirebaseDatabase: DatabaseReference
    var item_list: ArrayList<AttendanceModel> = ArrayList()
    var eventId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendance)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mFriendList = findViewById(R.id.myRecycleViewA)

        eventId = intent.getStringExtra("eventId")

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("attendance").child(eventId)


        mFirebaseDatabase.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                item_list.clear()
                if (p0.exists()) {
                    val children = p0.children
                    children.forEach {
                        val item: AttendanceModel = it.getValue(AttendanceModel::class.java)!!
                        item_list.add(item)
                    }
                    setupAdapter(item_list)
                }


            }

        })
    }


    private fun setupAdapter(data: ArrayList<AttendanceModel>) {
        val linearLayoutManager = LinearLayoutManager(this)
        myRecycleViewA.layoutManager = linearLayoutManager
        myRecycleViewA.adapter = AttendanceAdapter(data) {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
