package com.geeksdobyte.picnic

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import com.google.firebase.messaging.FirebaseMessaging
import org.jetbrains.anko.onClick


class Dashboard : AppCompatActivity() {
    lateinit var eventImage: ImageView
    lateinit var chatImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        FirebaseMessaging.getInstance().subscribeToTopic("all")

        val intent = intent
        val message = intent.getStringExtra("message")
        if (!message.isNullOrEmpty()) {
            AlertDialog.Builder(this)
                    .setTitle("Notification")
                    .setMessage(message)
                    .setPositiveButton("Ok", { dialog, which -> }).show()
        }

        eventImage = findViewById(R.id.homeEvents)

        chatImage = findViewById(R.id.homeChat)

        eventImage.onClick {

            val intent = Intent(this, EventsActivity::class.java)
            startActivity(intent)
        }


    }


}
