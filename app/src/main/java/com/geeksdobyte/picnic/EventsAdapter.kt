package com.geeksdobyte.picnic

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_list.view.*

class EventsAdapter(val messages: ArrayList<EventModel>, val itemClick: (EventModel) -> Unit) :
        RecyclerView.Adapter<EventsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.layout_list, parent, false)
        return ViewHolder(view, itemClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindForecast(messages[position])
    }

    override fun getItemCount() = messages.size

    class ViewHolder(view: View, val itemClick: (EventModel) -> Unit) : RecyclerView.ViewHolder(view) {

        fun bindForecast(message: EventModel) {
            with(message) {
                itemView.evName.text = message.ename
                itemView.evDate.text = message.edate
                itemView.evTheme.text = message.etheme
                Picasso.with(itemView.context).load(eventUrl)
                        .into(itemView.UserImageView)
                itemView.setOnClickListener { itemClick(this) }
            }
        }
    }
}