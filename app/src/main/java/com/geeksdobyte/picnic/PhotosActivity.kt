package com.geeksdobyte.picnic

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.WindowManager
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase


class PhotosActivity : AppCompatActivity() {

    lateinit var mFloatBtn: FloatingActionButton
    lateinit var mFriendList: RecyclerView
    lateinit var mFirebaseDatabase: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)


        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("photos")

        mFloatBtn = findViewById(R.id.floatingActionButton)

        mFloatBtn.setOnClickListener {
            val postIntent = Intent(applicationContext, AddPhotos::class.java)
            startActivity(postIntent)
        }


    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
