package com.geeksdobyte.picnic

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.WindowManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_single_event.*

class SingleEventActivity : AppCompatActivity() {


    var eventName: String = ""
    var eventLocation: String = ""
    var eventDate: String = ""
    var eventStart: String = ""
    var eventEnd: String = ""
    var eventTheme: String = ""
    var eventPhoto: String = ""
    var eventEid: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_event)
        eventName = intent.getStringExtra("eventName")
        eventLocation = intent.getStringExtra("eventLocation")
        eventDate = intent.getStringExtra("eventDate")
        eventEnd = intent.getStringExtra("eventEnd")
        eventStart = intent.getStringExtra("eventStart")
        eventTheme = intent.getStringExtra("eventTheme")
        eventPhoto = intent.getStringExtra("eventPhoto")
        eventEid = intent.getStringExtra("eventId")


        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)



        Picasso.with(this).load(eventPhoto)
                .into(imageViewE)

        seName.text = eventName
        seLocation.text = eventLocation
        seDate.text = eventDate
        seStart.text = eventStart
        seEnd.text = eventEnd
        seTheme.text = eventTheme


        buttonAttend.setOnClickListener { attendEvent() }
        buttonAttendList.setOnClickListener {
            val intent = Intent(this@SingleEventActivity, AttendanceActivity::class.java)
            intent.putExtra("eventId", eventEid)
            startActivity(intent)
        }


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }


    fun attendEvent() {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val uref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        uref.orderByKey().addListenerForSingleValueEvent(itemListener)


    }

    var itemListener: ValueEventListener = object : ValueEventListener {
        var myName: String = ""
        var photoUrl: String = ""
        val uid = FirebaseAuth.getInstance().uid ?: ""
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            // Get Post object and use the values to update the UI
            val map = dataSnapshot.value as HashMap<String, Any>
            myName = map.get("username") as String
            photoUrl = map.get("profileImageUrl") as String
            val ref = FirebaseDatabase.getInstance().getReference("/attendance/").child(eventEid).child(uid)
            ref.setValue(UserEv(uid, myName, photoUrl))
                    .addOnSuccessListener {

                    }
                    .addOnFailureListener {
                        Log.d(RegisterActivity.TAG, "Failed to set value to database: ${it.message}")
                    }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            // Getting Item failed, log a message
            Log.w("MainActivity", "loadItem:onCancelled", databaseError.toException())
        }
    }

}

class UserEv(val uid: String, val username: String, val photourl: String)