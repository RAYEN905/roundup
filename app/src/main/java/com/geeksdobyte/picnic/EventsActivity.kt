package com.geeksdobyte.picnic

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import android.view.WindowManager
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_events.*

class EventsActivity : AppCompatActivity() {

    lateinit var mFloatBtn: FloatingActionButton
    lateinit var mFriendList : RecyclerView
    lateinit var mFirebaseDatabase: DatabaseReference
    var item_list: ArrayList<EventModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_events)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mFloatBtn = findViewById(R.id.floatingActionButton)

        mFloatBtn.setOnClickListener {
            val postIntent = Intent(applicationContext, CreateEventActivity::class.java)
            startActivity(postIntent)
        }
        mFriendList = findViewById(R.id.myRecycleView)

        mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("events")


        mFirebaseDatabase.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                println(p0.message)
            }

            override fun onDataChange(p0: DataSnapshot) {
                item_list.clear()
                if (p0.exists()) {
                    val children = p0.children
                    children.forEach {
                        val item: EventModel = it.getValue(EventModel::class.java)!!
                        item_list.add(item)
                    }
                    setupAdapter(item_list)
                }


            }

        })


    }

    private fun setupAdapter(data: ArrayList<EventModel>) {
        val linearLayoutManager = LinearLayoutManager(this)
        myRecycleView.layoutManager = linearLayoutManager
        myRecycleView.adapter = EventsAdapter(data) {
            val intent = Intent(this@EventsActivity, SingleEventActivity::class.java)
            intent.putExtra("eventName", it.ename)
            intent.putExtra("eventLocation", it.elocation)
            intent.putExtra("eventDate", it.edate)
            intent.putExtra("eventStart", it.estime)
            intent.putExtra("eventEnd", it.eetime)
            intent.putExtra("eventTheme", it.etheme)
            intent.putExtra("eventPhoto", it.eventUrl)
            intent.putExtra("eventId", it.euid)
            startActivity(intent)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}

